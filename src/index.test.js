let test = require('tape');
const maxTotal = require('./index');

test("Maximum total equals single element in row", (t) => {
  t.equal(maxTotal([[2]]), 2);
  t.end();
});

test("It calculates the maximum total in the triangle", (t) => {
  
  t.equal(maxTotal([[1],[1,2]]), 3);
  t.equal(maxTotal([[6], [3,5],[9,7,1],[4,6,8,4]]), 26);
  t.equal(maxTotal([[1], [1, 3], [6, 1, 6]]), 10);
  t.equal(maxTotal([[2], [3,2], [0,1,1], [3,6,8,4], [5, 8, 1, 30, 10]]), 44);
  t.equal(maxTotal([[100], [0,-1], [2,1,0], [0,1,2,0], [0, 7, 1, 5, 9]]), 110);
  t.equal(maxTotal([[100], [-1,-2], [2,1,0], [0,1,2,0], [0, 7, 1, 5, 9]]), 109);
  t.equal(maxTotal([[0],[0,0],[0,0,0],[0,0,0,0], [0, 0, 0, 0, 0]]), 0);
  t.end();
});

test("Returns 0 for empty array entry", (t) => {
  t.equal(maxTotal([]), 0);
  t.end();
})