It requires node.js version 8.5.0 installed.
In the main directory execute on the command line:
```
npm install && npm test
```
It will run all solution's automated tests in the `src/index.test.js` file.
