function maxTotal(triangle) {
  let i = 0;
  let j = 1;
  const firstRow = triangle[0];
  let acc = firstRow && firstRow.length >= 1 ? firstRow[0] : 0;
  for (let index = 1; index < triangle.length; index++) {
    const row = triangle[index];
    if (row[i] > row[j]) {
      acc += row[i];
      j = i + 1;
    } else {
      acc += row[j];
      i = j;
      j = j + 1;
    }
  }
  return acc;
}

module.exports = maxTotal